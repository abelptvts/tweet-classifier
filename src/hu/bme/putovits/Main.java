package hu.bme.putovits;

import java.util.*;

/**
 * Naive Bayes Classifier (74.27% accurate)
 * https://en.wikipedia.org/wiki/Naive_Bayes_classifier
 * https://en.wikipedia.org/wiki/Naive_Bayes_spam_filtering
 * */

public class Main {

    private static final int NUM_TWEETS = 80000;

    enum Classification {
        NEGATIVE, POSITIVE
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<ArrayList<String>> classifiedTweets = getClassifiedTweets(scanner);
        ArrayList<Classification> classifications = getClassifications(scanner);
        HashMap<String, HashMap<Classification, Integer>> model = trainModel(classifiedTweets, classifications);

        int positiveCount = 0;
        int negativeCount = 0;
        for (Classification classification : classifications) {
            if (classification.equals(Classification.POSITIVE)) {
                positiveCount++;
            } else {
                negativeCount++;
            }
        }

        ArrayList<ArrayList<String>> tweets = getTweets(scanner);
        for (ArrayList<String> tweet : tweets) {
            Classification classification = classify(tweet, model, positiveCount, negativeCount);
            System.out.println(classification.ordinal());
        }
    }

    private static ArrayList<ArrayList<String>> getClassifiedTweets(Scanner scanner) {
        ArrayList<ArrayList<String>> tweets = new ArrayList<>(NUM_TWEETS);
        String line;
        for (int i = 0; i < NUM_TWEETS; i++) {
            assert scanner.hasNext();

            line = scanner.nextLine();
            tweets.add(parseTweet(line));
        }

        return tweets;
    }

    private static ArrayList<Classification> getClassifications(Scanner scanner) {
        ArrayList<Classification> classifications = new ArrayList<>();
        String line;
        for (int i = 0; i < NUM_TWEETS; i++) {
            assert scanner.hasNext();

            line = scanner.nextLine();

            if (line.isEmpty()) {
                continue;
            }

            Classification classification;
            if (Integer.valueOf(line) == 1) {
                classification = Classification.POSITIVE;
            } else {
                classification = Classification.NEGATIVE;
            }

            classifications.add(classification);
        }

        return classifications;
    }

    private static HashMap<String, HashMap<Classification, Integer>> trainModel(
            ArrayList<ArrayList<String>> tweets,
            ArrayList<Classification> classifications) {
        HashMap<String, HashMap<Classification, Integer>> model = new HashMap<>();

        assert tweets.size() == classifications.size();

        ArrayList<String> tweet;
        Classification classification;
        for (int i = 0; i < tweets.size(); i++) {
            tweet = tweets.get(i);
            classification = classifications.get(i);

            for (int j = 0; j < tweet.size(); j++) {
                String phrase = tweet.get(j);
                insert(phrase, classification, model);

                if (j + 1 < tweet.size()) {
                    // let's use a two word phrase now (increased accuracy from 0.68 to 0.74)
                    phrase += tweet.get(j + 1);
                }

                insert(phrase, classification, model);
            }
        }

        return model;
    }

    private static void insert(
            String phrase,
            Classification classification,
            HashMap<String, HashMap<Classification, Integer>> model) {
        if (!model.containsKey(phrase)) {
            model.put(phrase, new HashMap<>());
            model.get(phrase).put(Classification.POSITIVE, 0);
            model.get(phrase).put(Classification.NEGATIVE, 0);
        }

        Integer count = model.get(phrase).get(classification);
        model.get(phrase).put(classification, count + 1);
    }

    private static ArrayList<ArrayList<String>> getTweets(Scanner scanner) {
        ArrayList<ArrayList<String>> tweets = new ArrayList<>();
        String line;

        assert scanner.hasNext();

        do {
            line = scanner.nextLine();
            tweets.add(parseTweet(line));
        } while (scanner.hasNext());

        return tweets;
    }

    private static Classification classify(
            ArrayList<String> tweet,
            HashMap<String, HashMap<Classification, Integer>> model,
            int positiveCount,
            int negativeCount) {
        assert positiveCount != 0 && negativeCount != 0;

        double sum = Math.log((double) positiveCount / negativeCount);

        for (int i = 0; i < tweet.size(); ++i) {
            String word = tweet.get(i);
            String phrase = null;

            if (i + 1 < tweet.size()) {
                phrase = word + tweet.get(i + 1);
            }

            if (phrase != null && model.containsKey(phrase)) {
                sum += getStats(phrase, model);
            } else if (model.containsKey(word)) {
                sum += getStats(word, model);
            }
        }

        return sum > 0 ? Classification.POSITIVE : Classification.NEGATIVE;
    }

    private static double getStats(
            String phrase,
            HashMap<String, HashMap<Classification, Integer>> model) {
        int positives = model.get(phrase).get(Classification.POSITIVE);
        int negatives = model.get(phrase).get(Classification.NEGATIVE);

        if (positives == 0 || negatives == 0) {
            return 0;
        }

        return Math.log((double) positives / negatives);
    }

    private static ArrayList<String> parseTweet(String line) {
        ArrayList<String> tweet = new ArrayList<>();

        String[] split = line.split("\t");
        if (split.length > 1 || !split[0].equals("")) {
            tweet.addAll(Arrays.asList(split));
        }

        return tweet;
    }

}
